import { createServer } from 'http';
import { join, isAbsolute, normalize } from 'path';
import { app, net, protocol } from 'electron';
import { resolve } from 'app-root-path';

const devServer = async (directory: string, port?: number): Promise<void> => {
    // We need to load it here because the app's production
    // bundle shouldn't include it, which would result
    // in an error
    const nextServer = require('next')({ dev: true, dir: directory });
    const requestHandler = nextServer.getRequestHandler();

    // Build the renderer code and watch the files
    await nextServer.prepare();

    // But if developing the application, create a
    // new native HTTP server (which supports hot code reloading)
    const server = createServer(requestHandler);

    server.listen(port || 8000, () => {
        // Make sure to stop the server when the app closes
        // Otherwise it keeps running on its own
        app.on('before-quit', () => server.close());
    });
};

const adjustRenderer = (directory: string) => {
    const paths = ['/_next', '/static'];
    const isWindows = process.platform === 'win32';

    protocol.handle('file', (request): Response | Promise<Response> => {
        let path = request.url.substring(isWindows ? 8 : 7);

        for (const prefix of paths) {
            let newPath = path;

            // On windows the request looks like: file:///C:/static/bar
            // On other systems it's file:///static/bar
            if (isWindows) {
                newPath = newPath.substring(2);
            }

            if (!newPath.startsWith(prefix)) {
                continue;
            }

            // Strip volume name from path on Windows
            if (isWindows) {
                newPath = normalize(newPath);
            }

            newPath = join(directory, 'out', newPath);
            path = newPath;
        }

        // Electron doesn't like anything in the path to be encoded,
        // so we need to undo that. This specifically allows for
        // Electron apps with spaces in their app names.
        path = `${isWindows ? 'file:///' : 'file://'}${decodeURIComponent(path)}`;

        return net.fetch(path, { bypassCustomProtocolHandlers: true });
    });
};

interface Directories {
    production: string;
    development: string;
}

const index = async (directories: Directories | string, port?: number): Promise<void> => {
    if (!directories) {
        throw new Error('Renderer location not defined');
    }

    if (typeof directories === 'string') {
        directories = {
            production: directories,
            development: directories
        };
    }

    for (const directory in directories) {
        if (!Object.hasOwnProperty.call(directories, directory)) {
            continue;
        }

        if (!isAbsolute(directories[directory as keyof Directories])) {
            directories[directory as keyof Directories] = resolve(directories[directory as keyof Directories]);
        }
    }

    if (app.isPackaged) {
        adjustRenderer(directories.production);
        return;
    }

    await devServer(directories.development, port);
};

export default index;
